---
title: Л
layout: post
next: /public/morskoy_slovarik/m
previous: /public/morskoy_slovarik/k
---

Л  
   
Лаг — 1) прибор для определения скорости хода судна и пройденного расстояния; 2) борт корабля. Стать лагом — значит стать бортом к волне.  
Леер — туго натянутый трос, служащий для ограждения открытых мест.  
Лиман – затопляемая морем территория, устье реки.  
Линьки — смоленые веревки с узлом на конце, которые применяли в царском флоте для телесных наказаний матросов.  
Лисель — дополнительный парус, который ставится по бокам прямых парусов.  
Лот — прибор для определения глубины моря.  
Лоция — руководство для плавания в определенных, чаще близко к берегу расположенных местах.  
Люк — отверстие в палубе для схода вниз.