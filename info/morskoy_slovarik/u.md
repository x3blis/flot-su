---
title: У
layout: post
next: /public/morskoy_slovarik/v
previous: /public/morskoy_slovarik/t
---

У  
   
Узел — мера скорости движения корабля. Число узлов указывает на то, сколько морских миль прошел в течение часа корабль.  
Унтер-офицер — младший командир в царском флоте.