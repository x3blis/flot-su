---
title: Ю
layout: post
next: /public/morskoy_slovarik/z
previous: /public/morskoy_slovarik/ya
---

Ю  
   
Юнга — мальчик-ученик, готовящийся стать матросом.  
Ют — кормовая часть верхней палубы. Полуют — задняя, приподнятая часть юта.