---
title: З
layout: post
next: /public/obshie_svedenya/baltiyskiy-flot
previous: /public/morskoy_slovarik/yu
---

З  
   
Задраить — плотно при помощи задраек (болты с гайками) закрыть люк, иллюминатор и т.п.  
Зарифленный парус — парус с уменьшенной площадью.  
Зюйдвестка — непромокаемая морская шляпа с широкими полями.