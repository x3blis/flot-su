---
title: Загоскин Лаврентий Алексеевич
layout: post
next: /public/flotovodtsy/zavadovskiy-ivan-ivanovich
previous: /public/flotovodtsy/yanovskiy-mihail-iosifovich
---

Загоскин Лаврентий Алексеевич  
Лаврентий Алексеевич Загоскин – русский исследователь Аляски, отставной капитан-лейтенант. Родился в 1807 году, умер в 1859 году. Происходил из старинного дворянского рода.   
  
![](/assets/img/Zagoskin_Lavrentiy.gif)  
  
Воспитывался в Морском кадетском корпусе, и 17 мая 1823 года был произведен в гардемарины. В 1826 году на фрегате "Проворный" плавал до берегов Англии, и по окончании путешествия произведен в мичманы, с назначением в астраханский порт, где пять лет служил на судах каспийской флотилии, а затем получил должность адъютанта портового командира и был произведен в лейтенанты. С 1833 по 1835 год командовал пароходом "Аракс", который в 1835 году сгорел. Вследствие этого командир был предан суду и по Высочайшей конфирмации разжалован в матросы до выслуги, без лишения дворянства. Вскоре, однако, чин ему был возвращен, и 6 апреля того же года Загоскин был переведен в Кронштадт и находился здесь слишком три года, а потом (в 1838 году) поступил на службу Российско-Американской компании.   
На компанейских судах Лаврентий Загоскин совершил переход из Охотска в Ново-Архангельск, командовал бригом "Байкал" и кораблем "Елена", состоял начальником береговой экспедиции по исследованию течения рек Букланда и Квикпака и описывал прибрежную часть Северной Америки от Михайловского редута. В 1847 году Загоскин произведен в капитан-лейтенанты, а в 1848 году был уволен от службы и, находясь в отставке до конца жизни прожил в Рязанской губернии.   
В записках гидрографического департамента напечатана его статья: "Редут Святого Михаила в южной части Нертонова залива", и отпечатана отдельным изданием "Пешеходная опись части русских владений в Америке, произведенная в 1842—1844 годах". Кроме того, в "Сыне Отечества" за 1836 год были помещены его "Воспоминания о Каспии" и в декабрьской книжке "Русской Старины" "Воспоминания о морском шляхетском корпусе" (1822—1826 годы), в котором он отстаивает честь этого учреждения. "Воспоминания", состоящие, впрочем, из описания отдельных, незначительных самих по себе эпизодов, характерны не только для воспитавшего его учебного заведения, но и для самого автора. Старина, традиции корпуса и весь склад жизни дореформенной крепостнической Руси, отражающиеся в этом маленьком уголке, с ее патриархальными отношениями и экзекуциями — для него священны и симпатичны.  
   
 