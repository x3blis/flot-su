---
title: Берг Аксель Иванович
layout: post
next: /public/flotovodtsy/berh-moric-borisovich
previous: /public/flotovodtsy/berezkin-vsevolod-aleksandrovich
---

Берг Аксель Иванович  
Аксель Иванович Берг — советский ученый, инженер-адмирал, академик Академии Наук СССР с 1946 года; член-корреспондент с 1943года; Герой Социалистического Труда (1963). Родился 29 октября (10 ноября) 1893 года в Оренбурге.   
  
![](/assets/img/berg.gif)  
  
В качестве штурмана подводной лодки принимал участие в 1-й мировой войне 1914—18; в период гражданской войны — командир подводной лодки. Окончил Морской корпус (в 1914 году), Военноморское инженерное училище (в 1923 году), Военно-морскую академию в Ленинграде (в 1925 году), после чего вел преподавательскую и научную работу в высших военно-морских учебных заведениях. С 1926 — в Ленинском электротехническом институте.  
Аксель Иванович Берг принимал участие в первой мировой войне, в гражданскую войну был командиром подводной лодки. В 1922 ему присвоено звание "Герой труда отдельного дивизиона подводных лодок". В 1937 репрессирован, реабилитирован в 1940. В годы Великой Отечественной войны возглавил работы по радиолокации. В 1953—1957 годах Аксель Берг — заместитель министра обороны, с 1959 года— председатель Научного совета по комплексной проблеме "Кибернетика" при Президиуме АН СССР.   
Основные труды Берга посвящены разработке теорий и методов проектирования и расчета ламповых генераторов, стабилизации частоты, вопросам усиления и управления колебаниями ламповых генераторов. Выдвинул и разработал ряд проблем (о сеточном детектировании, о расчете генератора с искаженной формой импульса анодного тока и др.), имеющих важное значение для развития радиотехники. Берг является автором большого числа учебников по радиотехническим специальностям: "Общая теория радиотехники" (1925), "Теория пустотных генераторов переменного тока" (1925), "Основы радиотехнических расчетов" (часть 1, 1928, 2 изд.. 1930), "Теория и расчет ламповых генераторов" (1932, 2 изд. 1935) и другие.   
Большую роль играет научно-организаторская работа Берга на посту председателя Всесоюзного научного совета по радиофизике и радиотехнике Академии Наук СССР, председателя правления Всесоюзного научно-технического общества радиотехники и электросвязи имени А. С. Попова. В 1951 году Бергу была присуждена Академией Наук СССР золотая медаль имени А. С. Попова.  
   
 