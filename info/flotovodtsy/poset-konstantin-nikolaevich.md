---
title: Посьет Константин Николаевич
layout: post
next: /public/flotovodtsy/rikord-petr-ivanovich
previous: /public/flotovodtsy/pereleshin-pavel-aleksandrovich
---

Посьет Константин Николаевич  
Константин Николаевич Посьет — генерал-адъютант, адмирал. Родился в 1819 году. Его предок (в русских документах Пoccиem, или Пассет, или Пассиет и прочее), по приглашению Петра Великого приехал в Россию из Франции и много лет трудился над разведением виноградников около Астрахани.   
  
![](/assets/img/poset.gif)  
  
Окончив курс в Морском кадетском корпусе, Посьет плавал в морях Белом и Балтийском. Труд его: "Артиллерийское учение" (1847), рассматривающий правила артиллерийской практики иностранных флотов, имел влияние на увеличение боевой силы наших военных судов, что и не замедлило сказаться в битвах наших у берегов Кавказа. Другой труд Константина Николаевича: "Вооружение военных флотов" (1849, 2-е издание 1851 года) был увенчан Демидовской премией. Назначенный командиром учебного артиллерийского корабля "Прохор", Посьет применил на нем все придуманные им улучшения.   
Когда на адмирала Путятина возложено было заключение торгового договора с Японией, Посьет отправился с ним на фрегате "Паллада" (плавание которого описал И. А. Гончаров); позже он вновь ездил в Японию для обмена трактатов. В чине капитана 1 ранга Посьет назначен был состоять наставником при великом князе Алексее Александровиче и с тех пор постоянно совершал летом морские плавания (с 1858 по 1865 год) по Балтийскому морю и у берегов России. С 1866 по 1868 год Посьет ходил, вместе со своим Августейшим воспитанником, в Средиземное море и Атлантический океан, а в 1871 году посетил Америку, Канаду, Китай, Японию, Сибирь. Этим плаванием завершилось морское образование великого князя, к которому, до его совершеннолетия, Посьет назначен был попечителем.   
С 1874 года по 1888 год Посьет был министром путей сообщения. В этом звании он организовал особые "описные партии", работы которых послужили к исправлению и улучшению многих наших рек; для облегчения судоходства на внутренних водах назначены моряки в судоходные инспекции; для тех же целей учреждены водомерные посты, метеорологические станции и судоходные съезды; произведено много гидротехнических работ.   
При Посьете выстроено 9085 километров железнодорожных путей, выработан и утвержден общий устав российских железных дорог, положено начало урегулированию железнодорожных тарифов, расширены каналы Очаковского бара и Керчь-Еникальского пролива, устроены новые каналы Сяский, Свирский, Мариинский, открыт Морской канал в Петербурге, произведено множество промеров наших гаваней и рек. С 1888 года Посьет состоит членом государственного совета. Участвовал в учреждении общества спасания на водах и постоянно следил за развитием этого дела. По его мысли на наших реках и озерах устроены зимние спасательные посты.