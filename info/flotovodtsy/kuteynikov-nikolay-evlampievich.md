---
title: Кутейников Николай Евлампиевич
layout: post
next: /public/flotovodtsy/kuznecov-nikolay-gerasimovich
previous: /public/flotovodtsy/kushelev-grigoriy-grigorevich
---

Кутейников Николай Евлампиевич  
Николай Евлампиевич Кутейников — корабельный инженер, известный своими работами по постройке флота в период от введения брони и парового двигателя до русско-японской войны. Родился в 1854 году, образование получил в Инженерном и артиллерийском  училище морского ведомства и в Офицерских классах, преобразованных затем в Морскую академию.   
Первые годы своей службы провел под руководством адмирала Попова, участвуя в разработке его проектов судов, и состоял преподавателем Инженерного училища. В 1878—1879 годах, участвуя в составе американской экспедиции капитан-лейтенанта Семечкина, руководил постройкой в Филадельфии, на заводе В. Крампа, быстроходного крейсера "Забияка" и переделкой в крейсера коммерческих пароходов.   
Будучи строителем фрегата "Дмитрий Донской" в Санкт-петербургском адмиралтействе, Николай Кутейников предложил и разработал много деталей, принятых и для последующих судов флота (привод для заваливания S-образных шлюпбалок, крышки горловин и прочего). Проведя затем постройку броненосца "Император Николай Первый" на заводе Франко-Русского общества в качестве правительственного инженера, Кутейников в 1890 году был приглашен на службу в морской технический комитет, где в качестве инспектора и главного инспектора кораблестроения прослужил до 1905 года, руководя проектированием и постройкой многих военных судов; при его непосредственном участии был создан проект первого русского крейсера дальнего плавания "Рюрик".   
Кутейников явился автором ценза для оценки деятельности корабельных инженеров и положения о вознаграждении их за постройку судов. Под руководством Николая Евлампиевича воспитывались теоретически и практически многие поколения кораблестроителей, а также получили специальное морское техническое образование Великие Князья Константин и Дмитрий Константинович и Александр Михайлович. Николай Евлампиевич Кутейников умер в 1906 году в чине генерал-лейтенанта.