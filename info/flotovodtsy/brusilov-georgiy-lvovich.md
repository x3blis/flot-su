---
title: Брусилов Георгий Львович
layout: post
next: /public/flotovodtsy/brusilov-lev-alekseevich
previous: /public/flotovodtsy/bredal-petr-petrovich
---

Брусилов Георгий Львович  
Георгий Львович Брусилов - (родился в 1884 году, скончался в 1914 году) — русский исследователь Арктики, лейтенант, штурман. В 1909 году участвовал в подготовке Северной гидрографической экспедиции.   
После плавания в 1911 году у северных берегов Сибири на одном из судов гидрографической экспедиции Северного Полярного моря Брусилов увлекся мыслью стать во главе полярной промысловой экспедиции. Для этой цели в 1912 году им было приобретено в Англии прочное деревянное судно (паровая шхуна), названное "Святой Анной". Экспедиция Брусилова имела задачей повторить плавание Норденшельда и пройти вдоль берегов Сибири из Атлантического океана в Великий.   
Выйдя 28 июля 1912 года из Петербурга с экипажем в 24 человека и обогнув Скандинавию, судно экспедиции в начале сентября вошло в Карское море, очень скоро было затерто льдом и начало дрейфовать в северном направлении. Дрейф этот продолжался непрерывно 542 дня — до 10 апреля 1914 года, когда штурман В. И. Альбанов с 13 матросами оставил судно, в надежде по льду достигнуть обитаемой земли.   
Из этой партии 12 человек погибли, и только двое — сам Альбанов и матрос Кондрат — через 90 дней достигли архипелага Земли Франца-Иосифа, где были подобраны судном экспедиции Седова "Святой Фока" и доставлены на Мурманск. Дрейф "Святой Анны" с остальным экипажем, во главе с Брусиловым, продолжался дальше. Организованные в 1914 году поиски экспедиции не привели ни к каким результатам.  
   
 