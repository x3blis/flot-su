---
title: Колчак Александр Васильевич
layout: post
next: /public/flotovodtsy/kornilov-vladimir-alekseevich
previous: /public/flotovodtsy/kocebu-otto-evstafevich
---

Колчак Александр Васильевич  
Гидролог, полярный исследователь. Адмирал (звание получил в 1918 году), один из руководителей Белого движения. Выпускник Морского кадетского корпуса (1894 год). Участник Русской полярной экспедиции барона Э. В. Толля (1900—1902 годы), Гидрографической экспедиции Северного Ледовитого океана (1909—1910 годы), действительный член Русского Географического общества (с 1906 года).  
  
![](/assets/img/Kolchak.gif)  
  
Александр Васильевич родился 4 ноября 1874 года, в селе Александровском Петербургского уезда Петербургской губернии, в дворянской семье, умер 7 февраля 1920 года, в Иркутске. Он был одним из первых сотрудников образованного в 1906 году Морского Генерального штаба (с 1910 года - начальник Балтийского оперативного отдела Морского Генштаба), эксперт Государственной Думы по военно-морским вопросам. В годы первой мировой войны — офицер действующего флота, награжден орденом Святого Георгия 4-й степени. В годы революции — активный противник большевиков. Военный и морской министр правительства Директории, затем Верховный правитель России (ноябрь 1918 — январь 1920 года). В январе 1920 года был передан в руки большевиков и расстрелян.