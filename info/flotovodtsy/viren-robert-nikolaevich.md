---
title: Вирен Роберт Николаевич
layout: post
next: /public/flotovodtsy/vrangel-ferdinand-petrovich
previous: /public/flotovodtsy/velikiy-knyaz-konstantin-nikolaevich
---

Вирен Роберт Николаевич  
Роберт Николаевич Вирен — вице-адмирал, родился в 1856 году, воспитанник морского училища, произведен в мичманы в 1878 году, в 1880—1883 годах плавал за границей, в 1884 году окончил Минный офицерский класс, до 1889 года плавал в Балтийском море, а в 1889—1891 годах за границей.   
  
![](/assets/img/Viren.gif)  
  
Назначенный в 1891 году преподавателем минного дела к Великому Князю Георгию Александровичу, Вирен до 1894 года находился в Абастумане. В 1894 году произведен за отличие в кап. 2 ранга, плавал в Балтийском море и за границей до 1896 года, в 1901 году произведен в капитаны 1 ранга и назначен командиром крейсера "Баян", строившегося во Франции.   
Летом 1903 года, командуя крейсером, совершил при тревожных политических обстоятельствах переход на Восток, за что получил Монаршее благоволение. За проявленное мужество в морском бою 27 января 1904 года Вирен награжден был золотой саблей, а за выход 31 марта в море для спасения тонувшего миноносца "Страшный", во время которого "Баяну" под огнем неприятельских крейсеров удалось спасти из воды несколько матросов миноносца, получил орден святого Георгия 4 степени.   
Дальнейшая деятельность Вирена в Порт-Артуре выразилась в руководстве работами по тралению рейда и в выходах для обстрела береговых позиций неприятеля. С возвращением части эскадры, после боя 28 июля, в Порт-Артур он был назначен флигель-капитаном к контр-адмиралу князю Ухтомскому, которого в августе заместил, вступив в должность командующего отрядом судов, находившихся в Порт-Артуре. 23 августа 1904 года Роберт Вирен произведен за отличие в контр-адмиралы. Состояние отряда в это время было таково, что Вирен не счел возможным повторить попытку прорыва и все средства вверенных ему судов обратил на укрепление сухопутного фронта.   
В конце ноября, после потери Высокой горы, Вирен был ранен осколками снаряда в обе ноги и спину, вследствие чего должен был съехать на берег. 16 декабря на военном совете, собранном генерал-адмиралом Стесселем для обсуждения вопроса о пределе обороны крепости, Вирен, как и большинство участников совета, высказался за продолжение обороны; но когда, 3-мя днями позже, генерал-адъютант Стессель, решив сдать крепость, известил адмирала о том, что для порчи судов остается всего одна ночь, Вирен приказал взрывать и топить все еще уцелевшие суда. Один броненосец "Севастополь", вопреки этому приказанию, был выведен своим командиром на глубокую воду.   
После сдачи Порт-Артура Вирен добровольно пошел в плен. По окончании войны он был назначен младшим флагманом в Черном море, затем переведен в Балтийское и в 1906 году назначен начальником артиллерийского отряда, а в 1907 году исполняющий должности главного командиpa Черноморского флота; пробыв на этом посту около года, Вирен был назначен членом Адмиралтейского Совета, а в 1909 году главным командиром Кронштадского порта; в том же году произведен в вице-адмиралы.