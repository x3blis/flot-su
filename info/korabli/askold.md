---
title: «Аскольд»
layout: post
next: /public/korabli/avrora
previous: /public/korabli/arktika
---

«Аскольд»  
Крейсер «Аскольд» был заложен в 1899 году в городе Киль. В 1900 году он был спущен на воду, а спустя два года вошел в состав Балтийского флота. В связи с напряжённой обстановкой на Дальнем Востоке «Аскольд» был переведён на Дальний Восток, пополнив Тихоокеанскую эскадру.   
Характеристики: водоизмещение нормальное 5905 т; длина 130 м, ширина 15 м, осадка 6 м, скорость хода 23 узла, дальность плавания 3140 миль. Вооружение: 12 152-мм, 12 75-мм, 2 63-мм, 8 47-мм и 2 37-мм орудия, 2 пулемета, 6 торпедных аппаратов. Экипаж 534 человека.   
  
![](/assets/img/Askold .gif)  
  
Уже в первые дни русско-японской войны «Аскольд» в составе русской тихоокеанской эскадры вступил в бой с японскими кораблями. За время базирования в Порт-Артуре крейсер совершил пять боевых выходов. 28 июля 1904 года «Аскольд» вместе с эскадрой попытался прорваться во Владивосток. Получив несколько пробоин, он всё же сумел прорваться сквозь кольцо японских кораблей. Из-за серьёзных повреждений крейсер не дотянул до Владивостока и остановился в Шанхае, где находиться до окончания войны.   
Прибыв после окончания военных действий во Владивосток, «Аскольд» был включён в состав Сибирской флотилии, а после вступления России в первую мировую войну переведён в Средиземное море. Принимал участие в Дарданелльской операции, заслужив хвалебные отзывы союзников.  
В марте 1916 года «Аскольд» прибыл во французский Тулон для ремонта. Там среди матросов распространились революционные настроения. Узнавшее об этом командование устроив провокацию (взрыв снаряда на артиллерийском складе), обвинило в происшедшем матросов-революционеров, четверо из которых были расстреляны. В 1917 году крейсер, закончивший ремонт, прибыл в Мурманск. В 1918 году большая часть экипажа крейсера была демобилизована. 14 июля 1918 года крейсер был захвачен английским интервентами, а остававшиеся на нём члены экипажа под конвоем высажены на берег. «Аскольд» был разграблен, разоружен и уведен в Англию. В ноябре 1921 года крейсер возвратили Советской России, но за ветхостью он был продан на металлолом.  
 