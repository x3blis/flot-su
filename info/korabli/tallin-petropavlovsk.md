---
title: «Таллин» («Петропавловск»)
layout: post
next: /public/korabli/tashkent
previous: /public/korabli/t-205
---

«Таллин» («Петропавловск»)  
Тяжелый крейсер был построен в Германии и отбуксирован к Ленинграду в 1940 году. Ему было дано название «Петропавловск». Длина судна равнялась 212 м, ширина – 21,7 м, осадка – 7,24 м. Полное водоизмещение судна составляло 19 040 т. На вооружении «Петропавловска» находились четыре 203-мм и двенадцать 37-мм орудий.  
  
![](/assets/img/tallin.gif)  
  
К началу Великой Отечественной войны крейсер был еще не полностью готов, ни одно из его помещений не было достроено, но наравне с остальными кораблями Балтийского флота он вышел на защиту Родины. В сентябре 1941 года, когда немецкие войска подошли к Ленинграду, «Петропавловск» оказывал артиллерийскую поддержку советским сухопутным войскам. В эти дни крейсер обрушил на врага около 700 снарядов. 17 сентября 1941 года немецкая артиллерия обстреляла крейсер 210-мм снарядами. От полученных пробоин судно затонуло.    
В сентябре 1942 года крейсер был поднят и отбуксирован вверх по Неве. К концу декабря этого же года судно было отремонтировано, оснащено новой артиллерией и вернулось в ряды Балтийского флота. Впоследствии оно получило имя «Таллин». В 1944 году крейсер участвовал в снятии блокады Ленинграда.   
В 1953 году тяжелый крейсер «Таллин» был переклассифицирован в несамоходное учебное судно. В конце 50-х годов 20 века он был списан и разобран.