---
title: «Пролетарий»
layout: post
next: /public/korabli/s-13
previous: /public/korabli/principium
---

«Пролетарий»  
Канонерская лодка «Пролетарий» первоначально называлась «Вотяк». В 1907 году она была доставлена в разобранном виде на Дальний Восток, собрана и спущена на воду. Эксплуатация лодки началась в 1908 году.   
Длина канонерской лодки равна 54,5 м, ширина – чуть более 8 м, осадка 1 м. Полное водоизмещение 338 т, нормальное – 318 т. При мощности 480 лошадиных сил лодка могла развивать скорость до 23 км/ч по течению и 13,4 км/ч против течения. На вооружении лодки состояли два 100-мм и три 37-мм орудия, четыре 12,7-мм пулемета и около 30 мин заграждения.  
  
![](/assets/img/proletariy.gif)  
  
В 1914 году лодка была разобрана и сдана на хранение, в сентябре 1918 года корпус лодки захватили японские интервенты. В 1925 году корабль был возвращен Советскому Союзу. Два года спустя канонерская лодка вошла в строй и получила новое имя «Пролетарий». В 1929 году лодка вошла в состав Дальневосточной флотилии.   
Во время Русско-японской войны 1945 года «Пролетарий» в составе Краснознаменной Амурской флотилии обеспечивал продвижение войск 2-го Дальневосточного фронта на среднем Амуре. Под артиллерийским огнем противника он высадил на берег десант с противотанковыми средствами и одновременно с этим обстреливал врага. В 1945 году канонерская лодка «Пролетарий» была удостоена гвардейского звания. В 1949 году она была разоружена и списана.