---
title: «Селемджа»
layout: post
next: /public/korabli/sevastopol
previous: /public/korabli/s-56
---

«Селемджа»  
Канонерская лодка «Селемджа» была заложена в 1939 году на судостроительном заводе в Гамбурге. На воду судно было спущено в 1940 году, эксплуатация началась 12 апреля 1941 года. Первоначально «Селемжа» была паровой грунтоотвозной шаландой, только с началом Великой Отечественной войны она была переоборудована как канонерская лодка.  
Длина лодки равна 59,5 м, ширина – почти 12 м, осадка – 3,6 м. Полное водоизмещение лодки равнялось 1 140 т. Паровая машина мощностью 800 лошадиных сил позволяла развивать скорость до 8,5 узла. После переоборудования в июле 1941 года на вооружении лодки состояли два 130-мм, два 37-мм и одно 245-мм орудия и один пулемет. Экипаж канонерской лодки состоял из 104 человек.  
16 июля 1941 года «Селемжа» вместе с другими кораблями была переведена в состав Ладожской военной флотилии. Канонерская лодка неоднократно участвовала в боях с фашистами, поддерживала операции сухопутных войск. В июле-августе 1941 года доставляла десанты на острова Лункулансари и Мантсинсари в Ладожском озере, прикрывала боевые действия 168-й стрелковой дивизии в районе реки Тулоксы, обеспечивала эвакуацию нескольких подразделений на остров Валаам и остров Марьянсари.  
В 1942 году «Селемжа» участвовала в обороне Ленинграда, доставляла в блокированный город продовольствие, топливо, боеприпасы, бойцов Красной Армии. Во время несения дозорной службы канонерская лодка уничтожила несколько вражеских артиллерийских батарей и огневых точек. В 1944 году участвовала в разгроме немецких войск на Карельском перешейке.   
После окончания Великой Отечественной войны «Селемжа» снова была переоборудована в мирное судно и передана Балттехфлоту. В 1974 году лодка была выведена из эксплуатации и разобрана.