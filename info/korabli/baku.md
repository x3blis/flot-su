---
title: «Баку»
layout: post
next: /public/korabli/bka-213
previous: /public/korabli/azov
---

«Баку»  
Эсминец «Киев» был заложен 15 января 1935 года, спущен на воду 25 июля 1938 года. 6 мая 1940 года корабль был включен в состав Тихоокеанского флота. 27 декабря 1939 года эсминец переименован в «Серго Орджоникидзе», а 25 сентября 1940 года — в «Баку». Характеристики: водоизмещение 2350 т, длина 127,5 м, ширина 11,7 м, осадка 4,2 м, скорость хода 42 узла, дальность плавания до 2000 миль. Вооружение: пять 130-мм, два 76,2-мм и шесть 37-мм орудий, шесть 12,7-мм пулеметов, два четырехтрубных 533-мм торпедных аппарата, 2 параван-трала. Принимал на борт 68 якорных мин заграждения, 34 больших и 40 малых глубинных бомб. Экипаж 311 человек.  
В период с 15 июля по 14 октября 1942 года «Баку» в составе эскадры особого назначения совершил переход из Владивостока в Полярное, и был включён в состав 1-го дивизиона эскадренных миноносцев Северного флота. В ноябре 1942 года во время боевого похода эсминец попал в сильный шторм, и лишь грамотные действия экипажа позволили ему остаться на плаву и самостоятельно добраться до базы для ремонта.   
  
![](/assets/img/baku.gif)  
  
20 января 1943 года эсминцы «Баку» и «Разумный» вышли в море для перехвата вражеского конвоя, обнаруженного разведкой. Около полуночи советские корабли обнаружили два транспорта и два корабля охранения. Советским эсминцам удалось взорвать один из транспортов, остальные корабли сумели отойти под прикрытие береговых батарей, после чего «Баку» с «Разумным» отправились на базу.   
За годы Великой Отечественной войны «Баку» провел без потерь 29 конвоев, четырежды выходил в море для поиска и уничтожения его транспортных средств, принимал участие в огневой поддержке сухопутных войск, отражении атак авиации противника. 6 марта 1945 «Баку» был награжден орденом Красного Знамени. В 1959 году эсминец был переоборудован в плавказарму, а через несколько лет исключен из списков флота.   
 