---
title: «Минин»
layout: post
next: /public/korabli/mo-302
previous: /public/korabli/metel
---

«Минин»  
Броненосный фрегат «Минин» был заложен в 1864 году одновременно с «Князем Пожарским». В ходе строительства «Минин» начал подвергаться многочисленным переделкам, поэтому строительство его сильно затянулось. Спуск на воду состоялся только в 1869 году, но работы по его совершенствованию проводились до 1874 года.   
Характеристики корабля: длина – 90 м, ширина – 15 м, осадка – 6,7, водоизмещение - 5940 т, скорость - 12,4 узла. Вооружение: четыре 203 мм, двенадцать 152 мм, четыре 87 мм, два 64 мм, восемь 47 мм, четыре 37 мм. Экипаж 535 человек Бронирование - 140 мм броневой пояс, 38 мм броневая палуба.  
  
![](/assets/img/Minin.gif)  
  
«Минин» вступил в строй лишь 14 лет спустя после его закладки. На тот момент он являлся одним из лучших крейсеров мира. С 1892 года крейсер не покидал вод Балтики. В 1898 г. на крейсер установили паровую машину с черноморского парохода «Опыт», парусный рангоут заменили легкими мачтами, оборудовали новые жилые и учебные помещения. Из артиллерии к 1900 г. на «Минине» остались лишь четыре 203-мм орудия на поворотных станках Вавассера и две 64-мм пушки, добавили шесть 152-мм 45-калиберных и шесть 75-мм орудий, восемь 47-мм и четыре 37-мм одноствольных пушки, четыре пулемета.   
В1906 г. «Минин» был переведён в класс учебных кораблей, а в 1908 г. - в класс минных заградителей. В 1909 г. бывший броненосец получил новое название «Ладога». Перед первой мировой войной заградитель участвовал в постановке центрального минного заграждения в Финском заливе. В 1915 году корабль, возвращаясь после установки очередного заслона, сам подорвался на минном заграждении, поставленном германской подводной лодкой у Гангутского полуострова.   
    