---
title: «Императрица Екатерина II»
layout: post
next: /public/korabli/ingermanland
previous: /public/korabli/hrabryy
---

«Императрица Екатерина II»   
        
Броненосец Екатерина II был заложен 22 сентября 1884 года. Корабль был спущен на воду 6 мая 1886 года. В 1892 году переведён в класс эскадренных броненосцев. Характеристики корабля: длина – 103,5 м, ширина – 21 м. Осадка – 8,6 м, водоизмещение - 11048 т, скорость - 15 узлов. Вооружение: шесть 305мм, семь 152мм, восемь 47мм, четыре 37мм, семь торпедных аппаратов. Экипаж корабля - 633 человека Бронирование - 203-406 мм броневой пояс, 57 мм броневая палуба, 305 мм каземат, 229 мм боевая рубка.  
  
![](/assets/img/Ekaterina2.gif)  
  
В 1882 году была поставлена задача изготовить броненосец для черноморского флота. Изначально предполагалось построить его аналогичным уже существующим кораблям. Однако в итоге броненосец так и не стал аналогом британского «Аякса», французского «Каймана» или российского «Петра Великого».   
20 декабря 1882 г. Морским Министерством был утверждён общий чертёж нового броненосца.  В феврале 1883 г. были разработаны детальные чертежи расположения машин, котлов, артиллерии, но усовершенствование конструкций корабля продолжалось по мере его строительства. Трехцилиндровые машины двойного расширения были заказаны Балтийскому заводу, а броня и орудийные станки — в Англии.   
Всего было выпущено 4 корабля серии «Екатерина II»: «Георгий Победоносец», «Чесма», «Синоп» и сама «Екатерина II». Броненосец «Императрица Екатерина» был исключён из списков флота в октябре 1906 года.