---
title: «Усыскин»
layout: post
next: /public/korabli/varyag
previous: /public/korabli/tuman
---

«Усыскин»  
В 1934 году новый, только что построенный буксирный пароход был назван именем «Усыскин» в честь русского воздухоплавателя И.Д. Усыскина. По решению Государственного Комитета Обороны в 1941 году было семь буксиров, которые были переделаны в канонерские лодки, эта учесть постигла и пароход «Усыскин». Осенью 1941 года после установки вооружения корабль вступил в строй и стал заниматься подготовкой специалистов для действующих флотов и флотилий.   
Технические характеристики канонерской лодки «Усыскин»: нормальное водоизмещение 400 тонн; длина 56,4 метров, ширина 8 метров, осадка 1,25 метра; мощность паровой машины 480 лошадиных сил; скорость хода 10 узлов; дальность плавания 1400 миль. Вооружение: 2 100-мм и 2 45-мм орудия, 1 12,7-мм и 1 7,62-мм пулемет. Экипаж 76 человек.  
  
![](/assets/img/Usyskin.gif)  
  
Во время Сталинградского сражения канонерская лодка«Усыскин» начал свой боевой путь. Первый боевой залп корабль произвел 18 сентября 1942 года. Своим огнем лодка обеспечила защиту частей 62-й и 66-й армий, сдерживавших яростный натиск противника.   
Участвуя в боевых действиях под Сталинградом, «Усыскин» провел в общей сложности 212 артиллерийских стрельб, уничтожив свыше 650 вражеских солдат и офицеров, 19 танков, 51 автомашина, 5 артиллерийских и 9 минометных батарей, 4 отдельных орудия, 5 складов с боеприпасами, горючим и другим имуществом.   
4 мая 1943 года корабль «Усыскин» в районе Александровского переката подорвался на вражеской мине, но смог дойти до базы для ремонта. Однако повреждения корпуса были настолько сильны, что 21 июня 1943 года корабль был разоружен и исключен из списков Военно-Морского Флота. Но имя корабля было передано другой канонерской лодке «Громов.   
 