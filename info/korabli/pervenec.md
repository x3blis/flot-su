---
title: «Первенец»
layout: post
next: /public/korabli/petr-velikiy
previous: /public/korabli/peresvet
---

«Первенец»  
«Первенец» был первым кораблём типа броненосной батареи, которые строились для преграждения доступа к Кронштадту неприятельского флота в случае попытки его прорвать его оборону между фортами в узком пространстве большого рейда. «Первенец» был заказан британскому Темзенскому железоделательному и судостроительному заводу 16 ноября 1861 г. Также в Англии, на заводе Модслея и Фильда была построена и трехцилиндровая паровая машина для корабля.   
Заказ военного корабля в Англии объяснялся неподготовленностью российской промышленности к производству кораблей подобного типа. Для того чтобы в дальнейшем избежать подобных казусов в Англию направили несколько русских инженеров и мастеров, которые должны были изучить технику производства броненосцев. В дальнейшем на российских предприятиях строились корабли не хуже британских.  
  
![](/assets/img/Pervenets.gif)  
  
Длина корпуса корабля составляла 67 м, ширина 16 м. Борта судна были значительно (на 27°) загнуты внутрь, благодаря чему попадающие в броню снаряды рикошетили от корпуса. Однако резкий уклон от ватерлинии сказался на внутреннем размещении и мореходных качествах броненосной батареи, поэтому в дальнейшем подобная конструкция не применялась. На носу и корме располагались таранные штевни.   
Корпус «Первенца» был разделен на семь отсеков водонепроницаемыми переборками, толщина которых составляла 13,5 мм. Броневая защита корабля начиналась от верхнего края борта, а заканчивалась в 1,2 м ниже ватерлинии. Толщина брони составляла 102-114 мм. Помимо паровой машины броненосная батарея имела трехмачтовое парусное вооружение. На фок-мачте располагались прямые, а на грот- и бизань-мачтах косые паруса. Экипаж корабля составлял 432 человека. Броненосная батарея «Первенец» находилась в строю до 1905 года.