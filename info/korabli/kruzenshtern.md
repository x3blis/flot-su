---
title: «Крузенштерн»
layout: post
next: /public/korabli/kursk
previous: /public/korabli/kreml
---

«Крузенштерн»  
Четырехмачтовый барк «Крузенштерн» был построен гамбургской пароходной компанией «Фридрих Лайеш» и спущен на воду в 1926 году. В то время барк назывался «Падуя», а название «Крузенштерн», в честь руководителя первого русского кругосветного плавания адмирала И. Ф. Крузенштерна, он получил в 1946 году, когда перешел к Советскому Союзу после Второй мировой войны. Корабль приписан к городу Калининград.  
Технические характеристики «Крузенштерна» составляют 114,5 м в длину и 14 м в ширину, осадка – 7 м, высота борта – около 8,5 м. Общая площадь парусов корабля равна 3,4 тысячи м². Корабль развивает скорость на парусах до 17,4 узлов, средняя скорость – 15 узлов. Корабль оснащен двумя двигателями мощностью 1600 лошадиных сил и развивает скорость 7 узлов. Водоизмещение порожнего корабля равно 3760 т, полностью загруженного – 5725 т.   
  
![](/assets/img/Kruzenshtern.gif)  
  
«Крузенштерн» является винджаммером, то есть самым быстроходным из всех парусных кораблей. До начала Первой Мировой войны, когда он еще назывался «Падуя», он осуществлял торговые рейсы между Европой, Южной Америкой и Австралией. В 20-30-х годах 20 века он поставил несколько рекордов: совершил рейс из Гамбурга до чилийского порта Талькауано за 87 дней и из Гамбурга до Порт-Линкольна в Австралии за 67 дней.  
В 1961 году корабль был оснащен двумя вспомогательными дизельными двигателями и мог идти даже в безветренную погоду, хотя и с меньшей скоростью. Начиная с 1974 года «Крузенштерн» участвует в международных парусных гонках, именно благодаря регатам корабль стал так известен в России и за рубежом. В 1992 году он занял первое место в гонке Бостон-Ливерпуль и установил собственный рекорд скорости – 17,4 узла.   
В настоящее время «Крузенштерн» принадлежит Балтийской государственной академии рыбопромыслового флота РФ, используется в учебных целях, участвует в парусных регатах. Капитаном «Крузенштерна» является О. К. Седов.