---
title: «Князь Пожарский»
layout: post
next: /public/korabli/knyaz-potemkin-tavricheskiy
previous: /public/korabli/kirov
---

«Князь Пожарский»  
В марте 1864 г . было принято решение о начале строительства броненосных кораблей, отличавшихся большей боевой мощью и повышенной мореходностью, чем броненосные батареи, создававшиеся в начале 60-х. Первым из них предстояло стать восьмипушечному бронированному корвету, получившему имя «Князь Пожарский» в честь выдающегося ополченца XVII века.  
Характеристики: длина - 82 м, ширина - 15 м, осадка - 5,6 м, водоизмещение - 4137 т. скорость (после модернизации) - 11,9 узлов. Экипаж - 494 человека. Толщина брони - 140 мм броневой пояс, 114 мм верхний пояс.  
  
![](/assets/img/Knyaz Pozharskiy.gif)  
  
Корабль был заложен 18 ноября 1864 г . Машины и котлы для броненосца заказали на заводе Берда. Спуск корабля на воду планировался в 1866 году, но некоторые изменения проекта позволили сделать это лишь 31 августа 1867 года. Однако уже в начале 1867 года броненосец был включён в состав флота.  
Являясь первым кораблём подобного типа «Князь Пожарский» достраивался и переделывался даже после вступления в строй. В 1871 г.  на фрегате установили над средним мостиком бронированное прикрытие толщиной 50 мм для защиты от ружейного огня в ближнем бою. Из-за значительной толщины брони и тяжёлых металлических мачт корабль не обладал хорошими мореходными качествами, поэтому дальней шее усовершенствование велось именно в этом направлении. В результате «Князь Пожарский» стал первым русским броненосным кораблем, вышедшим за пределы Балтики.   
В 1875 году крейсер подвергся очередной модернизации: корпус его покрыли однорядной деревянной обшивкой и цинковыми листами, установили восемь новых котлов, усовершенствовали паровую машину и поставили дополнительную трубу. В результате скорость судна возросла на один узел. Было усовершенствовано и вооружение корабля.  
В 1878 г . крейсер отправился в свой второй поход в Средиземное море, где находился в качестве станционера при посольстве в Пирее. В апреле 1880 г . «Князь Пожарский» был переправлен на Дальний Восток. Изначально предполагалось, что корабль останется в составе Сибирской флотилии, но в сентябре 1881 г. «Князь Пожарский» вернулся в Пирей, а затем в Кронштадт.   
Вскоре «Князь Пожарский» стал флагманским кораблем практической эскадры Балтийского моря. В 1887 г . на крейсере заменили котлы. В 1890 году вооружение корабля было дополнено двумя 2,5-дюймовыми десантными пушками системы Барановского, двумя 47-мм и пятью 37-мм пятиствольными револьверными пушками Гочкисса, одной 37-мм одноствольной для установки на марсах, а затем двумя 381-мм надводными торпедными аппаратами. Из крупных орудий на корабле осталась лишь устаревшая 6-дюймовая пушка. Несмотря на все усовершенствования уже к концу XIX века корабль морально и физически устарел. Некоторое время он использовался в качестве учебного корабля, а в апреле 1911 был официально исключён из списков флота.  
    