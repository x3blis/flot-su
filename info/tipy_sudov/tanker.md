---
title: Танкер
layout: post
next: /public/tipy_sudov/tender
previous: /public/tipy_sudov/shnyava
---

Танкер  
Танкер (от английского tank - цистерна, бак) – судно, предназначенное для транспортировки жидких грузов, которые заливаются в специальные резервуары большого объёма. Основными материалами, перевозимыми при помощи танкеров, являются: нефть и продукты её переработки, сжиженные газы, пищевые продукты и вода, химические продукты.  
  
![](/assets/img/suda/tanker.gif)  
  
Изначально жидкие вещества перевозили на грузовых судах исключительно в бочках. Лишь в конце XIX века распространился способ перевозки, подобный современному танкерному. Впервые перевозка наливом была проведена в России в 1873 на Каспийском море братьями Артемьевыми на деревянной парусной шхуне "Александр". Ощутив преимущества нового способа перевозки, на подобный способ перевозки начали переходить повсеместно. Довольно быстро грузоподъемность танкеров перевалила за 1000 тонн.  
Современный танкер представляет собой однопалубное самоходное судно с машинным отделением, жилыми и служебными помещениями в корме. Чтобы уменьшить вероятность разлива содержимого танкеры, как правило, делают с двойным дном. Грузовые помещения разделяются несколькими поперечными и продольными переборками.  
Залив груза ведётся береговыми средствами через специальные палубные горловины, разгрузку - судовыми насосами. Некоторые виды груза нуждаются в поддержании определённой температуры, поэтому в танках расположены специальные змеевики, по которым пропускается охладитель или нагреватель.    
Современные танкеры делят на несколько категорий, в зависимости от дедвейта (разности водоизмещения с полным грузом и без груза):  
• GP - малотоннажные танкеры (6000-16499 т)   
• GP - танкеры общего назначения (16500-24999 т)  
• MR - среднетоннажные танкеры (25000-44999 dwt)  
• LR1 - крупнотоннажные танкеры 1 класса (45000-79999 т)  
• LR2 - крупнотоннажные танкеры 2 класса (80000-159999 т)  
• VLCC - крупнотоннажные танкеры 3 класса (160000-320000 т)   
• ULCC - супертанкеры (более 320000 т).  
 