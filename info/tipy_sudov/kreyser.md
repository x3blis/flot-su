---
title: Крейсер
layout: post
next: /public/tipy_sudov/ladya
previous: /public/tipy_sudov/korvet
---

Крейсер  
Крейсер — общее название судов, большей частью быстроходных, способных долгое время пробыть на море, вооруженных легкой артиллерией (большей частью скорострельной) и сравнительно слабозащищенных. Под таким названием существуют суда самых разнообразных видов и размеров (водоизмещение от 300 тонн до 14000 тонн).  
  
![](/assets/img/suda/kreiser.gif)  
  
Назначение крейсера — охранять отечественную морскую торговлю, вредить неприятельской торговле, служить сторожевыми, рассыльными судами, разведчиками и т. д. О специальных крейсерах минных. В прежнее время назначение крейсера выполнялось отчасти фрегатами, затем корветами, бригами, шхунами. К концу XIX в России были крейсера двух разрядов (рангов) – броненосные и бронепалубные. Крейсеров I ранга в балтийском флоте было 12, в черноморском — 1.   
Главное оружие современного крейсера - нарезная артиллерия и ракетные комплексы. Также корабли могут быть вооружены зенитной артиллерией, торпедами и минами. На вооружении большинства современных кораблей имеются 1-2 лёгких самолёта. Взлёт самолётов осуществляется при помощи специальных устройств - катапульт, или 1-2 вертолёта, которые используются для разведки и корректирования огня.   
Размеры современного крейсера: длина до 200-220 м, ширина 20- 23 м, осадка до 8 м. Водоизмещение лёгких крейсеров 7-9 тыс. т, тяжёлых до 20-30 тыс. т. Экипаж от 600 до 1300 человек, скорость хода 55-65 км/ч.