---
title: Тральщик
layout: post
next: /public/tipy_sudov/triera
previous: /public/tipy_sudov/tender
---

Тральщик  
Тральщик — судно специального назначения, которое осуществляет поиск, обнаружение и уничтожение морских мин, а также проводку кораблей через минные заграждения. По водоизмещению, мореходности и вооружению различают несколько видов тральщиков: морские (водоизмещение 660 - 1300 тонн), базовые (водоизмещение до 600 тонн), рейдовые (водоизмещение до 250 тонн) и речные (до 100 тонн) тральщики.  
  
![](/assets/img/suda/tral.gif)  
  
По принципу действия различают контактные, акустические и электромагнитные тральщики. Контактные действуют следующим образом: специальными ножами подрезают минрепы (тросы) мин, а всплывающие мины расстреливают. Акустические тральщики при помощи специальных акустических средств имитируют прохождение крупного корабля, в результате чего мины взрываются. На похожем принципе основано действие электромагнитных тральщиков, имитирующих электромагнитное излучение цели.   
В настоящее время наблюдается развитие тральщиков электромагнитного типа.   
Процесс уничтожения включает несколько операций: поиск, обнаружение, классификацию и нейтрализацию мин. Современные противоминные корабли оснащаются гидроакустическими станциями, комплексом точной навигации и системами обработки и отображения информации.   
 