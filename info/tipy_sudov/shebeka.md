---
title: Шебека
layout: post
next: /public/tipy_sudov/shhuna
previous: /public/tipy_sudov/seyner
---

Шебека  
Шебека — длинное, узкое, остроконечное судно XVIII века для легкой военной службы и крейсирования, пришедшее на смену галерам. Имеет 3 мачты (передняя наклонена вперед). Длина шебеки составляла до 35 м.  
  
![](/assets/img/suda/shebeka.gif)  
  
Шебека впервые была применена в русском флоте во время Архипелажской экспедиции 1769–1774 гг. В конце XVIII в. судно вошло в состав Балтийского гребного флота и, достигнув значительных размеров, имело три мачты с косыми парусами, до 20 пар весел и от 30 до 50 пушек.   
   
Узкий длинный корпус с развалом бортов и сильно выдвинутым форштевнем обеспечивал кораблю хорошую мореходность. Шебека по конструкции корпуса была близка к каравеллам и галерам, но превосходила их по скорости, мореходности и вооружению. В задней части корабля строилась палуба, сильно выступающая на корму. Наибольшая ширина верхней палубы составляла около трети ее длины, форма подводной части была исключительно острой.  
 