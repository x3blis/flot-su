---
title: Бригантина
layout: post
next: /public/tipy_sudov/bronenosec
previous: /public/tipy_sudov/brig
---

Бригантина  
Бригантина— бриг малого размера. Это название присвоено одному виду легких судов Средиземного моря, на которых две или три мачты однодеревки с латинским вооружением. Паруса с реями можно спустить и положить вдоль судна и, выкинув 20 или 30 весел, идти под веслами. Суда эти употреблялись преимущественно пиратами.  
  
![](/assets/img/suda/brigantina.gif)  
  
В XVI—XIX веках бригантины, как правило, использовались пиратами. Позднее они преобразовались в двухмачтовые парусные суда с фок-мачтой, имеющей вооружение, как у брига, и грот-мачтой с косыми парусами, как у шхуны — грота-триселем и топселем. В XVIII веке были введены в военных флотах как посыльные и разведывательные корабли.  
 