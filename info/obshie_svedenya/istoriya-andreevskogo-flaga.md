---
title: История Андреевского флага
layout: post
next: /public/obshie_svedenya/kaspiyskaya-flotiliya
previous: /public/obshie_svedenya/flotskie-zvaniya
---

История Андреевского флага  
Главным символом Российского Флота с петровских времен является Андреевский флаг – полотно прямоугольной формы, на котором изображены две пересекающиеся голубые полосы, которые символизируют косой андреевский крест.  
Почему флаг называется андреевским?   
Флаг назван в честь апостола Андрея первозванного, который является покровителем России. Апостол Андрей был первым учеником Исуса Христа. Поэтому его и называют первозванным: Христос призвал первым в ученики именно Андрея – простого рыбака, который промышлял на Галилейском озере. Апостол Андрей был великим путешественником и проповедником христианской веры. Андрей – родной брат апостола Петра. Он отправился в путешествие по землям, которые населяли славянские племена. Он учил славян-язычников христианской вере.  
Апостол андрей побывал в Киеве и добрался до Новгорода, где на берегу озера Волхов установил большой крест. Полагают, что этот крест стоял там, где сейчас находится село Грузино. В честь этого в селе построен храм Андрея Первозванного. Апостол Андрей принял мученническую смерть: его подвергли распятию, как и Исуса Христа. Но апостол Андрей не согласился, чтобы его распяли на прямом кресте, сказав, что он недостоен быть распятым так же, как был распят Господь. Поэтому он принял смерть на косом кресте. Апостола Андрея почитают в России как ее покровителя и молитвенника.  
Петровская эпоха  
При Петре Первом в 1696 году был учрежден первый в России орден – орден апостола Андрея Первозванного. Орден является одной из самых высоких государственных наград и по сей день.  
При Петре Первом и появился андреевский флаг как символ Российского Флота. Было несколько рисунков этого флага, которые сделал сам царь Петр Первый. 11 декабря 1699 года Петр Первый учредил андреевский флаг как символ Российского Флота: «Флаг белый, через который синий крест св. Андрея того ради, что от сего апостола приняла Россия святое крещение».  
Дальнейшая история флага  
До 1917 года андреевский флаг был кормовым флаго всех кораблей российского флота. Петр Первый постановил: «Все корабли российские не должны ни перед кем спускать флага». После реваолюции 1917 года Андреевский флаг был запрещен советской властью. 17 января 1992 года постановлением Правительства России Андреевский флаг был возвращен Российскому Флоту как его изначальный символ.  
![](/assets/img/flags/image001.gif)  
  Флаг образца 1693 г.  
![](/assets/img/flags/image002.gif)  
  Флаг образца 1696 г.  
![](/assets/img/flags/image003.gif)  
  Флаг 1697-1699 гг.  
![](/assets/img/flags/image004.gif)  
  Флаг образца 1699 г.  
![](/assets/img/flags/image005.gif)  
  Белый флаг 1701-1710 гг.  
![](/assets/img/flags/image006.gif)  
  Синий флаг.  
![](/assets/img/flags/image007.gif)  
  Красный галерный флаг.  
![](/assets/img/flags/image008.gif)  
  Андреевский флаг 1710-1712 гг.  
![](/assets/img/flags/image009.gif)  
  Андреевский флаг.  
![](/assets/img/flags/image010.gif)  
  Гюйс.  
![](/assets/img/flags/image011.gif)  
  Флаг кораблей Морского корпуса.  
![](/assets/img/flags/image012.gif)  
  Флаг судов Учебного экипажа.  
![](/assets/img/flags/image013.gif)  
  Флаг "для лоции".  
![](/assets/img/flags/image014.gif)  
  Флаг гидрографических судов.  
![](/assets/img/flags/image015.gif)  
  Флаг судов Висленской военной флотилии.  
![](/assets/img/flags/image016.gif)  
  Георгиевский адмиральский флаг.  
![](/assets/img/flags/image017.gif)  
  Флаг военного транспорта 1804-1865 гг.  
![](/assets/img/flags/image018.gif)  
  Штандарт (до 1845 г.).  
![](/assets/img/flags/image019.gif)  
  Шлюпочный (до 1870 г.) флаг вице-адмирала.  
![](/assets/img/flags/image020.gif)  
  Шлюпочный (до 1870 г.) флаг контр-адмирала.  
![](/assets/img/flags/image021.gif)  
  "Ординарный" вымпел 1699-1870 гг.  
![](/assets/img/flags/image022.gif)  
  Георгиевский вымпел 1870-1917 гг.  
![](/assets/img/flags/image023.gif)  
  Синий вымпел.  
![](/assets/img/flags/image024.gif)  
  Красный вымпел.  
![](/assets/img/flags/image025.gif)  
  Георгиевский вымпел 1819-1870 гг.  
![](/assets/img/flags/image026.gif)  
  Андреевский вымпел.