---
title: Балтийский флот
layout: post
next: /public/obshie_svedenya/chernomorskiy-flot
previous: /public/morskoy_slovarik/z
---

Балтийский флот  
Создание Балтийского флота началось 18 мая 1703 году, когда Россия вела войну со Швецией за выход в Балтийское море. Балтийский флот можно назвать отцом всего Российского военно-морского флота. Корабли Балтийского флота неоднократно принимали участие в знаменитых морских сражениях: Гангутском, Чесменском, Наваринском и многих других.  
Во время Первой Мировой войны корабли Балтийского флота мужественно сразились с кораблями германского флота и не допустили их господство в Балтийском море. Во Великой Отечественной войны они обороняли Моонзундские острова, Таллин, полуостров Ханко, участвовали в героической обороне Ленинграда, поддерживание наступления советских войск в Прибалтике, Восточной Пруссии и Восточной Померании.  
В составе Балтийского флота в настоящее время находятся эскадренные миноносцы, сторожевые и ракетные корабли и катера, десантные, противолодочные корабли, подводные лодки, вспомогательные и поисково-спасательные суда, ВВС флота, береговые войска. Все корабли и суда вооружены новейшими комплексами ракетного, артиллерийского и торпедного оружия, системами ПВО.    
Корабли Балтийского флота базируются в городе Балтийск Калининградской области и в Кронштадте недалеко от Санкт-Петербурга. Командующим Балтийским флотом с 6 декабря 2007 года является вице-адмирал В. Мардусин.