---
title: Северный флот
layout: post
next: /public/obshie_svedenya/tihookeanskiy-flot
previous: /public/obshie_svedenya/kaspiyskaya-flotiliya
---

Северный флот  
Северный флот был образован 1 июня 1933 года как Северная военная флотилия и переименован 11 мая 1937 года. Флот, в основном, базируется в закрытом административно-территориальном образовании (ЗАТО) Североморск. С 20 ноября 2007 года командующим Северного флота является вице-адмирал Н. М. Максимов.  
В состав Северного флота входят:  
• тяжелый авианесущий крейсер «Адмирал флота Советского Союза Кузнецов»;  
• 2 тяжелых атомных ракетных крейсера «Адмирал Нахимов» и «Петр Великий»;  
• ракетный крейсер «Маршал Устинов»;  
• большие противолодочные корабли «Адмирал Чабаненко», «Вице-адмирал Кулаков», «Маршал Василевский», «Североморск», «Адмирал Левченко», «Адмирал Харламов»;  
• эскадренные миноносцы «Гремящий», «Адмирал Ушаков»;  
• корвет «Стерегущий»;  
• малые противолодочные корабли МПК-113, МПК-194 «Брест», МПК-196, МПК-197, МПК-203 «Юнга», МПК-130 «Нарьян-Мар», МПК-7 «Онега», МПК-14 «Мончегорск», МПК-59 «Снежногорск»;  
• малые ракетные корабли «Айсберг», «Накат», «Рассвет»;  
• морские тральщики «Владимир Гуманенко», «Комендор», «Машинист», «Новоуральск»;  
• большие тральщики БТ-97 «Полярный», БТ-454 «Петрозаводск», БТ-152 «Котельнич», БТ-111 «Авангард», БТ-226 «Коломна», БТ-211 «Ядрин»;  
• большие десантные корабли «Митрофан Москаленко», БДК-91 «Оленегорский горняк», БДК-182 «Кондопога», БДК-55 «Александр Отраковский», БДК-32, БДК-45 «Георгий Победоносец».  
Кроме этого, в составе Северного флота имеются 3 тяжелых ракетных подводных крейсера стратегического назначения, 8 ракетных подводных крейсера стратегического назначения, 6 атомных подводных лодок с крылатыми ракетами, 12 атомных торпедных подводных лодок, 6 атомных глубоководных станция, 8 дизельных торпедных подводных лодок и 20 вспомогательных судов.   
На сегодняшний день Северный флот считается самым мощным из всех военных флотов России.