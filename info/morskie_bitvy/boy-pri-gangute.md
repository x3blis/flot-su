---
title: Бой при Гангуте
layout: post
next: /public/morskie_bitvy/boy-pri-napoli-di-romaniya
previous: /public/morskie_bitvy/bitva-u-bosforskogo-proliva
---

Бой при Гангуте   
Война: Северная война 1700-1721 годов.  
Участники: Россия и Швеция.  
Место действия: Балтийское море.  
Крупнейшие морские сражения, во многом определившие исход Северной войны, произошли в 1714 году. Шведские суда дошли до Гельсингфорса и Ревеля, заградив таким образом путь русским галерам, дошедшим до местечка Твереминне, находящегося близ Гангута, по восточную сторону полуострова. Для отвлечения неприятеля предполагалось сделать нападение на него нашим корабельным флотом, но это было очень рискованным.  
  
![](/assets/img/Gangut.gif)  
  
Петр, прибывший из Ревеля и осмотревший весь полуостров, отыскал на нем низкий песчаный перешеек длиной примерно 2,5 км, на котором тотчас велел устраивать дорогу для перетаскивания галер на западную сторону полуострова. Но шведы, узнав об этом намерении, поставили у конца предполагаемой дороги отряд военных судов, приготовившийся выстрелами встретить перевозимые по этому пути галеры.   
Выгодная позиция, занятая шведами, останавливая дальнейшее движение нашего галерного флота, могла оказать весьма вредное влияние на успех всей кампании и принудить к отступлению армию, находящуюся в Финляндии под начальством М. М. Голицына. На помощь русским явился счастливый случай, которым они сумели воспользоваться.   
После полудня 25 июля, услышав пальбу на шведском флоте, Петр сам отправился на ближайший к неприятелю островок узнать о причине пальбы и увидел, что 14 судов отделились от флота и, по всей вероятности, идут в Твереминне, чтобы запереть там наш галерный флот. Ввиду такой страшной опасности, Петр на собравшемся военном совете настоял, чтобы тотчас приблизить галеры к Гангуту и часть их послать в обход шведского флота.   
Утром 26 июля, воспользовавшись штилем, удерживающим на месте корабли неприятеля, 20 русских галер под командою Змаевича двинулись к шведскому флоту и благополучно обошли его со стороны моря, на расстоянии пушечного выстрела. Вслед за ними, таким же образом и так же безвредно, прошли еще 15 галер. Всем галерам, обогнувшим Гангут, велено было немедленно идти и запереть отряд шведских судов, ожидающий спуска на воду перетаскиваемых по устраиваемой дороге галер. Между тем, шведский адмирал поспешил воротить отделившийся от его флота отряд и растянул линию кораблей до того места, по которому прошли наши галеры. Но, опасаясь обхода со стороны моря, он отодвинул корабли от берега и оставил здесь свободный проход, которым русские не замедлили воспользоваться.   
Утром 27 июля, при совершенном штиле, в стройном порядке, следуя одна за другой, двинулись все остальные наши галеры, держась так близко к берегу, как только позволяла глубина. Авангард вел генерал Вейде, кордебаталию сам Апраксин, а в ариергарде шел Голицын. Ближайшие к берегу шведские суда спешили сняться с якоря и на буксире приблизиться к берегу. Жестокая пальба открылась по нашим галерам, но все они, кроме одной, ставшей на мель и доставшейся шведам, успешно обогнули мыс и, присоединившись к отряду Змаевича, в свою очередь, атаковали стоявшие у строившейся дороги шведские суда, бывшие под начальством шаутбенахта Эреншельда.   
Отряд Эреншельда состоял из флагманского 18-пушечного фрегата Элефант, шести галер, вооруженных 12 и 14 пушками малого калибра и имевших по две пушки 18 или 36-фунтовые. Эти галеры, с фрегатом посредине, стояли в линии, фланги которой упирались в маленькие островки, за срединой линии находились три шхербота, имевшие от 4 до 6 пушек малых калибров.   
Русские галеры, построенные в три колонны, атаковали неприятеля, впереди была авангардия под начальством шаутбенахта Петра Михайлова. Два раза атакующие, приближаясь к неприятелю, должны были отступить; но при третьей атаке и свалке на абордаж шведы не выдержали натиска, и после отчаянной обороны все суда, одно за другим, спустили флаги. У неприятеля перебито более трети экипажа; все оставшиеся в живых, и в том числе израненный Эреншельд, взяты в плен.   
Победою при Гангуте обеспечивалось прочное занятие всей Финляндии, причем для нападения русских открывалось все балтийское прибрежье Швеции, не исключая и самого Стокгольма. Петр ясно сознавал важное значение Гангутской победы. Гангутскую победу с торжеством праздновали в Петербурге, куда с триумфом приведены были взятые у шведов суда; все участвовавшие в сражении офицеры и нижние чины награждены медалями, а шаутбенахт Петр Михайлов произведен в вице-адмиралы.   
 