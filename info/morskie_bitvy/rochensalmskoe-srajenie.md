---
title: Роченсальмское сражение
layout: post
next: /public/morskie_bitvy/sinopskoe-srajenie
previous: /public/morskie_bitvy/revelskiy-morskoy-boy
---

Роченсальмское сражение   
Война: Русско-шведская война 1788-1790 годов.  
Участники: Россия, Швеция.  
Место действия: Балтийское море, Фридрихсгамский залив, у Выборга.   
Около 10 часов утра 13 августа 1789 года русская эскадра под руководством обер-интенданта Балле, приблизившись к неприятелю, открыла огонь, но после пятичасового жестокого боя под сосредоточенными выстрелами значительно сильнейшего неприятеля наши поврежденные суда, одно за другим, начали выходить из линии, причем два судна были захвачены шведами. Скоро выстрелы наших судов стали редеть, и в шесть часов началось их общее отступление.   
  
![](/assets/img/Rochensalmskoe.gif)  
  
На севере эскадра Нассау, подойдя к Королевским воротам и найдя закрытый проход, долго оставалась под огнем неприятеля. Наконец, по другому мелкому проливу успело пробраться на рейд несколько наших канонерских лодок, а в седьмом часу, с огромными усилиями и потерей людей, удалось настолько разломать затопленные в Королевских воротах суда, что фарватером этим могли пройти галеры.   
Едва эскадра Нассау прорвалась на рейд, как победа наша сделалась несомненной: первыми отбиты были нами суда, взятые у Балле, а потом начали сдаваться и шведские. В темноте и дыму отчаянная битва ядрами, картечью, книпелями, даже ружейным огнем продолжалась до 2 часов ночи. Разбитые шведы отступили по направлению к Ловизе, долго и упорно преследуемые нашими галерами и канонерскими лодками. Среди сражения, когда исход его уже был очевиден, король приказал сжечь более 30 транспортов и мелких судов, стоявших на мелком рейде, ближе к устьям Кюмени.   
У неприятеля было взято 5 больших судов, 1 галера, 1 канонерская лодка и 2 госпитальных судна. В числе пленных было 37 офицеров и 1100 нижних чинов. Наша потеря заключалась в одной галере и канонерской лодке, погибших от взрыва, и 58 офицерах и тысяче нижних чинов убитых и раненых. Нассау-Зиген предлагал главнокомандующему Мусину-Пушкину сильным десантом, высаженным в тылу неприятеля, отрезать королю наступление, а нашей армией, атаковав в это же время шведов с фронта, заставить их положить оружие. Но король, узнав о намерении Нассау, защитил батареями пункты, более удобные для высадки десанта, и поспешил отступить к Ловизе, преследуемый нашими войсками.   
Спустя около недели в восточном устье Кюмени наши канонерские лодки взяли 5 шведских гребных судов и потопили 4 большие вооруженные неприятельские лодки, шедшие с десантом. Этим и кончились военно-морские действия в кампанию 1789 года.  
 