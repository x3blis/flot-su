---
title: Бой за Порт-Артур
layout: post
next: /public/morskie_bitvy/chesmenskaya-bitva
previous: /public/morskie_bitvy/boy-v-uste-nevy
---

Бой за Порт-Артур  
Война: Русско-японская война 1904-1905 годов.  
Участники: Россия, Япония.  
Место действия: Желтое море, Западно-Корейский залив у крепости Порт-Артур.  
Еще в 1898 году Россия взяла в аренду на 25 лет часть Ляодунского полуострова с крепостью Порт-Артур. Здесь предполагалось возвести укрепления и построить базу главных военно-морских сил на Тихом океане. К началу 1904 году здесь располагались Первая Тихоокеанская эскадра и часть кораблей Сибирской флотилии.  
  
![](/assets/img/POrt_artur(1).gif)  
  
Японцы планировали высадить десант на Ляодунский полуостров, чтобы начать боевые действия на суше, еще зимой, но были остановлены русским флотом. Высадка десанта численностью 35 тысяч человек началась только 22 апреля. К середине мая железнодорожное  сообщение и телеграфная связь русской армии и Порт-Артура были прерваны. В середине июня японцы начали осаду крепости. Корабли Тихоокеанской эскадры предприняли попытку прорваться во Владивосток, но были вынуждены вернуться в Порт-Артур.   
Осада крепости продолжалась. После того как русское командование отказалось сдать крепость, японцы 6 августа начали общий штурм, но захватить крепость сразу же им не удалось, и они перешли в длительной осаде. Порт-Артур был сдан врагу 20 декабря 1904 года после 329-тидневной осады. Русские корабли, отважно защищавшие его со стороны моря, были частично уничтожены во время осады, оставшиеся же корабли привели в полную негодность сами матросы и офицеры накануне капитуляции. Шесть миноносцев и несколько катеров с боями прорвались в китайские порты.